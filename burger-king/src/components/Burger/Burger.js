import React from 'react';
import classes from './Burger.css';
import BurgerIngrediant from './BurgerIngrediant/BurgerIngrediant'
const burger = (props) => {
    return(
        <div className={classes.Burger}>
            <BurgerIngrediant type={'bread-top'}/>
            <BurgerIngrediant type={'cheese'}/>
            <BurgerIngrediant type={'meat'}/>
            <BurgerIngrediant type={'bread-Bottom'}/>
        </div>
    )
};
export default burger;